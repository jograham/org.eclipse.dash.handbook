////
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: SOURCE
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
////

[[starting]]
== Starting an Open Source Project at the Eclipse Foundation

Before getting started, it's important to know what is required of {aForgeName} project. The Eclipse Foundation will take ownership of many aspects of the project to ensure that the project and its assets are managed in an open and vendor-neutral manner. This takes the form, for example, of the Eclipse Foundation retaining ownership of the project's trademarks on behalf of the community, and carefully managing who has write access on project resources such as source code repositories and distribution channels. {forgeName} projects are obligated to use certain <<project-resources-and-services,resources>> assigned to the project by the Eclipse Foundation and conform to logo and trademark guidelines. New project sponsors must engage in the process of transitioning an existing project with the intent to continue development of the project code and growth of the community and ecosystem around the project.

It's also important to know what new projects don't give up. The project team retains control of the project's direction by virtue of regular contribution to the project. The contributors to the project retain ownership of their contributions (those contributions are used under license by the project). Project leads are required to ensure that other individuals who present themselves to the project are given uniform opportunity to participate, but the project team gets to establish the rules for participation (within certain parameters). The project team is responsible for determining development methodology, establishing plans, etc. Existing owners of the project code retain their ownership.

[[starting-proposal]]
=== Project Proposal

{forgeName} open source projects start with a proposal that is made available to the community for review. At the end of the _community review_ period, we engage in a _creation review_, and then provision the project resources.

[graphviz, images/starting_overview, svg]
.An overview of the Project Creation Process
----
digraph {
	// Graph properties
	bgcolor=transparent
	
	// Nodes that define the key points in the process
	node [shape=box;style=filled;fillcolor=white;fontsize=12];
	proposal[label="Project Proposal", group=g1];
	community[label="Community Review", group=g1];
	review[label="Creation Review", group=g1];
	provision[label="Provision", group=g1]
	
	// Nodes that define things that we need
	node [shape=plaintext;fillcolor=transparent;fontsize=10]
	approval [label="EMO\nApproval"]
	trademark [label="Trademark\nReview"]
	mentors [label="Mentors"]
	paperwork [label="Committer\nPaperwork"]

	// Stitch the key points together
	proposal -> community -> review -> provision
	
	// Use grey lines to add in the things we need
	edge [color=grey]
	approval -> community
	mentors -> review
	trademark -> review
	paperwork -> provision
	
	// Force the trademark and mentors boxes to
	// be on either side of the main process points.
	// Do this by creating invisible lines that would
	// cross if they are on the same side.
	node[style=invis] ic
	edge [style=invis]
	trademark -> ic
	mentors->provision
}
----

Use the {createurl}[web form] to create a new project proposal. Instructions are provided on the form. All new proposals are created in _draft_ mode, and are accessible only by the original author and anybody designated as a project lead or committer in the proposal. Only those individuals designated as a project lead may edit the proposal.

[NOTE]
====
Keep track of the URL of the proposal. The website does not provide public links to the document until after the proposal is opened for community review. If you do lose trck of the URL, ask mailto:{emoEmail}[the EMO] for assistance.
====

A proposal must minimally include a description of the project, a declaration of scope, and a list of prospective members (project leads and committers) before we make it accessible to the public for _community review_.

When you feel that the proposal is ready, send a note to the mailto:{emoEmail}[Eclipse Management Organization] (EMO) requesting that the proposal be made available to the public for review. The EMO will review the proposal and may provide feedback before initiating the _community review_ period.

At the beginning of the _community review_ period, the EMO will announce the proposal on several channels (the {activityUrl}[Project Activity News] page, Twitter, the {proposalsForum}[Proposals Forum], blog post, and an email note to the Eclipse Foundation members and committers). The EMO will also open a record in the Eclipse Foundation's issue tracker -- an instance of Bugzilla -- to track the progress of the proposal; the proposal's author and project leads will be copied on that record.

A proposal will be open for community review for a minimum of two weeks.

The Eclipse Foundation holds the _trademark_ for all {forgeName} projects. Trademark assignment is undertaken prior to the creation of any new project. If you already have a trademark on your project name, that trademark must be assigned to the Eclipse Foundation. Be advised that trademark assignment can be a time-consuming process (it can take hours, days, or weeks depending on the circumstances surrounding the name). If you currently hold the trademark, you will be asked to complete a {trademarkTransferUrl}[Trademark Transfer Agreement].

The proposal must list at least one mentor from the Eclipse Architecture Council. Members of the Eclipse Architecture Council have considerable experience with Eclipse Foundation practices, and the {edpLink}. The EMO will engage directly with the Architecture Council to identify mentors as necessary. Mentors are assigned to the project through the incubation phase; they are released from their duties when the project <<release-graduation,graduates>>.

When the project name trademark has been secured, a mentor has been identified, and the proposal contents are finalized, the EMO will schedule a _creation review_. Reviews -- which run for a minimum of one week -- are scheduled twice a month, generally concluding on the first and third Wednesday of each month. The creation review may overlap with the _community review_ period.

[NOTE]
====
Creation reviews tend to always be successful. They should be considered low stress as the hard work has already been done in advance of the start of the review.
====

[[starting-provisioning]]
=== Provisioning

Following a successful creation review, the EMO will initiate the provisioning process. 

Provisioning starts with an email message being sent to each of the committers listed on the project proposal with instructions on how to engage in the <<paperwork,committer paperwork>> process. The exact nature of that paperwork depends on several factors, including the employment status of the individual and the Eclipse Foundation membership status of their employer.

[NOTE]
====
The provisioning process will not start until after the paperwork for at least one project committer is received and processed by the EMO Records Team. If you can be ready with the paperwork in time for the completion of the creation review, then we can move quickly through the provisioning process. 

When we initiate provisioning, committers will be sent an email with instructions; please don't send any paperwork in until after you receive those instructions.
====

When the paperwork is received and processed for one committer, the Eclipse Webmaster will begin the resources provisioning process. When that process is complete, the Eclipse Webmaster will provide information regarding the nature and coordinates of the various  <<project-resources-and-services,resources and services>> available to the project.

[[starting-after-provisioning]]
=== After Provisioning

Before pushing any code into an official {forgeName} project repository, the project team must submit the project's <<ip-initial-contribution,_initial contribution_>> of project code and wait for the Eclipse Intellectual Property (IP) Team to grant `checkin` of that contribution. 

Project teams interact with the Eclipse IP Team via the <<ip-ipzilla,IPZilla>> system using <<ip-cq,Contribution Questionnaires>> (CQs). Those members of the project team that are included in the CQ's _CC_ list will be informed of progress via email.
    
[WARNING]
====
The IP Team will indicate check-in permission by adding the `checkin` keyword and final approval by setting the state of the CQ to `approved`. *Do not* push any code into the project repository until after the Eclipse IP Team has indicated that you are ready to do so.
====

<<ip-third-party,Third-party content>> that is required by the project code must also be reviewed by the Eclipse IP Team. Project teams that are new to the process should start by raising no more than three CQs for third-party content in order to gain an understanding of the third-party review process before proceeding to enter all requests.

[graphviz, images/post-creation, svg]
.Post creation activities
----
digraph {
	// Graph properties
	bgcolor=transparent
	
	
	// Nodes that define the key points in the process
	node [shape=box;style=filled;fillcolor=white;fontsize=12];
	ic[label="Submit\nInitial Contribution", group=g1];	
	git[label="Push to Git", group=g1];		
	build[label="Build and Distribute\n(milestones)", group=g1];
	release[label="Release", group=g1];
	
	// Nodes that define things that we need
	node [shape=plaintext;fillcolor=transparent;fontsize=10]
	ip_checkin [label="IP Team\n\"Check-in\""]
	ip_approval [label="IP Team\nApproval"]
	

	// Stitch the key points together
	ic -> git -> build -> release;
	
	// Use grey lines to add in the things we need
	edge [color=grey]
	ip_checkin -> git
	ip_approval -> release
}
----

After the project code is pushed into the project repository, the project team can create and distribute _milestone_ builds for the first release. However, all intellectual property must be approved by the Eclipse IP Team before the project team can issue any official <<release,releases>>.

[[starting-project-phases]]
=== Project Phases

{forgeName} projects move through various phases. Projects in active phases (incubation or mature) engage in all the normal sorts of development activies: project committers write code, run builds, release software, court contribution, and seek to convert contributors into committers. All new projects start in the _incubation phase_. Projects tend to stay in the incubation phase for a few months (varies depending on the nature and composition of the project) and then move into the mature phase. 


[graphviz, images/phases, svg]
.An overview of the Project lifecycle Phases
----
digraph {
	// Graph properties
	bgcolor=transparent;
	rankdir=LR;
	rank=same;
	// Nodes that define the key points in the process
	node [shape=box;style=filled;fillcolor=white;fontsize=12];
	{
		proposal[label="Proposal\nPhase"];
		incubation[label="Incubation\nPhase"];
		mature[label="Mature\nPhase"];
		archived[label="Archived"];
	}
	
	edge [fontsize=10];
	proposal -> incubation  [xlabel="Creation\nReview"];
	incubation -> mature [xlabel="Graduation\nReview"];
	mature -> archived [label="Termination\nReview"];
	incubation -> archived;

	incubation:e -> incubation:e [label="Release"]
	mature:e -> mature:e [label="Release"]
}
----

Key project lifeycle events are gated by reviews; moving from the incubation phase to the mature phase must be, for example, preceded by a <<release-graduation,graduation review>>.

[[starting-incubation]]
==== Incubation Phase

A project in the incubation phase is said to be _incubating_.

The classification of a project in the incubation phase is not a statement about the quality of the project's code; rather, the incubation phase is more about the project team's progress in practicing the open and transparent processes described by the {edpLink} to establish the three communities (developers, adopters, and users) around the project.

Incubating projects are encouraged to produce milestone builds, make releases, and grow their community.

When the project code is ready (e.g. stable APIs) and the project team has learned to operate as an open source project according to the {edpLink}, the project may opt to _graduate_ (via <<release-graduation,graduation review>> into the _mature phase_.

[[starting-incubation-branding]]
===== Incubation branding

In order to alert potential consumers of the incubating nature, projects in the incubation phase must include _incubation branding_. 

.The Incubation Logo
image::images/incubating.png[width=200px]

The project team must:

* Display the incubation logo on their project web page (if they have one);
* Display the incubation logo on their project's primary download page;
* Include the word "incubation" in the filename of all downloadable files (when technically feasible) for builds and milestones; and
* When technically feasible, include the word "incubation" in features (e.g. about dialogs, feature lists, and installers).

There are no incubation branding requirements for general user interface elements.

[NOTE]
====
For projects that produce OSGi artifacts, include the word "incubation" in the _Bundle-Name_, feature names, and p2 repositories. The word "incubation" should not be included in technical namespaces (especially when it may result in confusion when the project leaves incubation). e.g. an OSGi bundle's _Bundle-SymbolicName_, or a Java package name.
====

[[starting-mature]]
==== Mature Phase

Most of the lifetime of {aForgeName} project is spent in the mature phase. A mature project is one that:

* Is a good open source citizen with open, transparent, and meritocratic behavior;
* Regularly and predictably releases IP clean extensible frameworks and exemplary tools; and
* Actively nurtures the three communities: developers, adopters, and users.

[[starting-archived]]
==== Archived

When a project has reached its logical conclusion, its resources are archived. Transition in the Archived (phase) is preceded by a termination review.

[[starting-faq]]
=== Frequently Asked Questions

[qanda]

Why are some committer (but not all) names rendered as links on project proposal pages? ::

When the list of committers provided for a new project proposal are rendered, a link to more information for each individual committer will be included when possible. The link will render, for example, when an individual already has a role on another existing Eclipse open source project (e.g. is a committer, project lead, PMC member, etc.). No link indicates that no existing project relationships exist for that individual.

How do I find Eclipse Architecture Council mentors? ::

You don't have to find them yourself. Focus on the content of the proposal. We can solicit mentors from the Eclipse Architecture Council after the proposal has been opened for community review.

What license(s) can I use for my project? ::
{forgeName} top level projects define the standard licensing for their projects. When a project has nonstandard licensing requirements, they may need to make an appeal to the Eclipse board of directors to request their approval. Connect with mailto:{emoEmail}[EMO] for assistance.

Can I change the proposal after it is posted? ::

Yes. The proposal can be changed any time before the start of the creation review.

When do I submit my code for review by the IP team? ::

Submit your code (initial contribution) for review after the project has been provisioned. The Eclipse Webmaster will let you know when provisioning is complete.

Does the new project have to use Git? ::

Yes. Git is the only source code management system that is currently permitted for new projects. 

Can I host my project code on GitHub? ::

{forgeName} projects can make use of <<resources-github,GitHub>>. Official project repositories must be hosted under an Eclipse Foundation-managed organization at GitHub. Official repositories are subject to the same intellectual property due diligence rules and processes that all Eclipse project repositories must follow. 

How long should I let my project incubate? ::

It depends. Community expectations are one factor. Team experience with open source is another. If your team is new to open source, it may make sense to stay in incubation a little longer than a seasoned team with a mature code base might. As a general rule, though, projects should plan to leave incubation within a year.
	
Does the mature project code that I'm bring to {forgeName} need to incubate? ::

Yes. All new projects start in the incubation phase. Remember that incubation is as much about the project team learning about how to operate as an open source project as it is about the project code. Project teams that "get it" can opt to exit incubation quickly (e.g. with their first release) if that makes sense for the team and the community.
	
What do all these terms (e.g. EMO) mean? ::

Please see the <<glossary,glossary>>.